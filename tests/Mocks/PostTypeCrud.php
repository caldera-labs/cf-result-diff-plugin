<?php


namespace calderawp\testing\resultDiffPlugin\Tests\Mocks;
use calderawp\testing\resultDiff\DB\Entity;
use calderawp\testing\resultDiffPlugin\Storage\TypeCRUD;

/**
 * Class PostTypeCRUD
 *
 * Storage system for testing, that doesn't store anything, or use WordPress
 *
 * @package calderawp\testing\resultDiffPlugin\Tests\Mocks
 */
class PostTypeCRUD implements TypeCRUD
{

    /** @inheritdoc **/
    public function lastOfType($type, $formId )
    {
        return $this->entityFactory( rand(), $type, $formId );
    }

    /** @inheritdoc **/
    public function create( Entity $entity )
    {
        return $entity;
    }

    /** @inheritdoc **/
    public function read( $ID )
    {
        return $this->entityFactory( $ID, 'Entry' );
    }

    /** @inheritdoc **/
    public function update( Entity $entity )
    {
        return $entity;
    }

    /** @inheritdoc **/
    public function delete( Entity $entity )
    {
        return true;
    }

    /**
     * Create entiry for testing
     *
     * @param $id
     * @param $type
     * @return Entity
     */
    public function entityFactory( $id, $type, $formId = null )
    {
        return new Entity(
            [
                'ID'                => $id,
                'type'              => $type,
                'hash'              => md5( substr( md5( mt_rand() ), 0, rand( 4, 2 ) ) ),
                'formHash'          => md5( substr( md5( mt_rand() ), 0, rand( 4, 2 ) ) ),
                'formId'            => ( $formId ) ? $formId : 'CF42',
                'reference'         => md5( substr( md5( mt_rand() ), 0, 7 ) )
            ]
        );
    }
}