<?php


namespace calderawp\testing\resultDiffPlugin\Tests;


use calderawp\interop\Events\Events;
use calderawp\testing\resultDiffPlugin\Capture;
use calderawp\testing\resultDiffPlugin\ComparableFactory;
use calderawp\testing\resultDiffPlugin\DBSystem;
use calderawp\testing\resultDiffPlugin\Exceptions\Exception;
use calderawp\testing\resultDiffPlugin\Plugin;
use calderawp\testing\resultDiffPlugin\Storage\PostTypeCRUD;

class PluginTest extends TestCase
{

    /**
     * Test that app has to be set before adding to app
     *
     * @covers Plugin::pluginLoaded()
     */
    public function testPluginLoadedInvalid()
    {
        $app = $this->appFactory();
        $plugin =  $this->pluginFactory();

        $this->expectException( Exception::class );
        $plugin->pluginLoaded( $app->getEventsManager() );

    }


    /**
     * Test setting app
     *
     * @covers Plugin::setApp()
     */
    public function testSetApp()
    {
        $app = $this->appFactory();
        $plugin =  $this->pluginFactory();
        $plugin->setApp( $app );
        //Real test is that this doesn't throw an exception
        $app->addPlugin( $plugin );
        $this->assertInstanceOf(
            get_class( $app ),
            $plugin->getApp()
        );
    }

    /**
     * Test we get same app instance out of getter, when using setter.
     *
     * @covers Plugin::getApp();
     * @covers Plugin::setApp();
     */
    public function testGetApp()
    {
        $app = $this->appFactory();
        $plugin =  $this->pluginFactory();

        $plugin->setApp( $app );
        $this->assertEquals( $app, $plugin->getApp() );
    }

    /**
     * Test container has() method reports what it has and does not has
     *
     * @covers Plugin::has()
     */
    public function testHas()
    {
        $plugin = $this->pluginFactory();

        $this->assertFalse( $plugin->has( 'HiRoy' ) );
        $this->assertTrue( $plugin->has( Plugin::OFFSETDB ) );
        $this->assertTrue( $plugin->has( Plugin::OFFSETEVENTS ) );
        $this->assertTrue( $plugin->has( Plugin::OFFSETCOMPARABLEFACTORY ) );
        $this->assertTrue( $plugin->has( Plugin::OFFSETPOSTYPECRUD ) );
    }

    /**
     * Test the right type of objects come out of the container
     *
     * @covers Plugin::get()
     * @covers Plugin::allowedOffsets()
     * @covers Plugin::setupContainer()
     */
    public function testGet()
    {
        $plugin = $this->pluginFactory();
        $plugin->setApp( $this->appFactory() );

        $this->assertTrue(
            is_object(
                $plugin->get( Plugin::OFFSETDB )
            )
        );
        $this->assertInstanceOf(
            DBSystem::class,
            $plugin->get( Plugin::OFFSETDB )
        );

        $this->assertTrue(
            is_object(
                $plugin->get( Plugin::OFFSETPOSTYPECRUD )
            )
        );
        $this->assertInstanceOf(
            PostTypeCRUD::class,
            $plugin->get( Plugin::OFFSETPOSTYPECRUD )
        );

        $this->assertTrue(
            is_object(
                $plugin->get( Plugin::OFFSETEVENTS )
            )
        );
        $this->assertInstanceOf(
            Events::class,
            $plugin->get( Plugin::OFFSETEVENTS )
        );


        $this->assertTrue(
            is_object(
                $plugin->get( Plugin::OFFSETCOMPARABLEFACTORY )
            )
        );
        $this->assertInstanceOf(
            ComparableFactory::class,
            $plugin->get( Plugin::OFFSETCOMPARABLEFACTORY )
        );


    }


    /**
     * Test that container always returns same instance
     *
     * @covers Plugin::get()
     * @covers Plugin::allowedOffsets()
     * @covers Plugin::setupContainer()
     */
    public function testContainerSingleton()
    {
        $plugin = $this->pluginFactory();
        $plugin->setApp( $this->appFactory() );
        $this->assertSame(
            $plugin->get( Plugin::OFFSETDB ),
            $plugin->get( Plugin::OFFSETDB )
        );

        $this->assertSame(
            $plugin->get( Plugin::OFFSETPOSTYPECRUD ),
            $plugin->get( Plugin::OFFSETPOSTYPECRUD )
        );

        $this->assertSame(
            $plugin->get( Plugin::OFFSETEVENTS ),
            $plugin->get( Plugin::OFFSETEVENTS )
        );

        $this->assertSame(
            $plugin->get( Plugin::OFFSETCOMPARABLEFACTORY ),
            $plugin->get( Plugin::OFFSETCOMPARABLEFACTORY )
        );
    }

    /**
     * Test events get registered when loading plugin
     *
     * @covers Plugin::pluginLoaded()
     * @covers Plugin::$capture
     * @covers Capture::addHooks()
     */
    public function testEventsRegistered()
    {
        $app = $this->appFactory();

        $this->assertFalse(
            $app->getEventsManager()->hasFilter(
                'caldera_forms_mailer'
            )
        );

        $this->assertFalse(
            $app->getEventsManager()->hasFilter(
                'init'
            )
        );

        $plugin = $this->pluginFactory();
        $plugin->setApp( $app );
        $app->addPlugin( $plugin );

        $this->assertTrue(
            $app->getEventsManager()->hasFilter(
                'caldera_forms_mailer'
            )
        );

        $this->assertTrue(
            $app->getEventsManager()->hasFilter(
                'init'
            )
        );
    }



}