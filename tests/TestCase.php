<?php


namespace calderawp\testing\resultDiffPlugin\Tests;


use calderawp\interop\InteropApp;
use calderawp\testing\resultDiffPlugin\DBSystem;
use calderawp\testing\resultDiffPlugin\Plugin;
use calderawp\testing\resultDiffPlugin\TestMail;
use calderawp\testing\resultDiffPlugin\Tests\Mocks\PostTypeCRUD;

abstract class TestCase extends \PHPUnit\Framework\TestCase
{
    use \calderawp\testing\resultDiff\Traits\MockData;

    /**
     * @return \calderawp\interop\InteropApp
     */
    protected function appFactory()
    {
        return new \calderawp\interop\InteropApp(
            new \calderawp\interop\ServiceContainer(),
            dirname(__FILE__),
            '0.1.1'
        );
    }

    /**
     * @return Plugin
     */
    protected function pluginFactory()
    {
        $plugin = new Plugin(
            dirname(__FILE__),
            '0.1.1'
        );

        $plugin->resetDBSystem(
            $this->dbSystemFactory()
        );

        return $plugin;
    }

    /**
     * Add interopWp plugin to interop app if needed
     *
     * @param InteropApp $app
     */
    protected function maybeAddInteropWP( InteropApp $app )
    {
        if (!$app->hasPluginByNamespace("calderawp\\interopWP\\")) {
            $interopWpPlugin = new \calderawp\interopWP\Plugin();
            $app->addPlugin($interopWpPlugin);
        }
    }
    /**
     * @return DBSystem
     */
    protected function dbSystemFactory()
    {

        return new DBSystem(
            new PostTypeCRUD()
        );
    }

    /**
     * @return Plugin
     */
    protected function pluginWithAppFactory()
    {
        $app = $this->appFactory();
        $plugin = $this->pluginFactory();
        $plugin->setApp( $app );
        $app->addPlugin( $plugin );
        return $plugin;
    }

    /**
     * @param $entryId
     * @return TestMail
     */
    protected function mailTestFactory($entryId, $formId )
    {

        $plugin = $this->pluginWithAppFactory();

        return new TestMail(
            $this->mockData( 'caldera_forms_mailer' ),
            $this->mockData( 'EmailData' ),
            $this->mockData( 'Form', [ 'ID' => $formId ] ),
            $entryId,
            $plugin,
            $plugin->get( Plugin::OFFSETDB )
        );

    }

}