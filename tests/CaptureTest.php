<?php


namespace calderawp\testing\resultDiffPlugin\Tests;


use calderawp\testing\resultDiffPlugin\Capture;
use calderawp\testing\resultDiffPlugin\ComparableFactory;
use calderawp\testing\resultDiffPlugin\DBSystem;
use calderawp\testing\resultDiffPlugin\Tests\Mocks\PostTypeCRUD;

class CaptureTest extends TestCase
{

    /**
     * Test event registration
     *
     * @covers Capture::addHooks()
     */
    public function testAddHooks()
    {
        $app = $this->appFactory();
        $this->assertFalse(
            $app->getEventsManager()->hasFilter(
                'caldera_forms_mailer'
            )
        );

        $this->assertFalse(
            $app->getEventsManager()->hasFilter(
                'init'
            )
        );

        $plugin = $this->pluginFactory();
        $plugin->setApp( $app );
        $capture = $this->captureFactory($plugin);

        $capture->addHooks();

        $this->assertTrue(
            $app->getEventsManager()->hasFilter(
                'caldera_forms_mailer'
            )
        );

        $this->assertTrue(
            $app->getEventsManager()->hasFilter(
                'init'
            )
        );

    }

    /**
     * Test that mailer filter doesn't alter first arg/doesn't make errors
     *
     * @covers Capture::mailer()
     */
    public function testMailer()
    {
        $app = $this->appFactory();
        $this->maybeAddInteropWP( $app );
        $plugin = $this->pluginFactory();
        $plugin->setApp( $app );


        $mailerData = $this->mockData( 'EmailData'  );
        $mailerSettings = $this->mockData( 'EmailSettings' );
        $mockForm =  $this->getMockForm();
        $mockForm[ 'ID' ] = 'CF42';
        $capture = $this->captureFactory($plugin);

        $this->assertSame(
            $this->mockData( 'caldera_forms_mailer' ),
            $capture->mailer(
                $this->mockData( 'caldera_forms_mailer' ),
                $mailerData,
                $mockForm->toArray(),
                rand()
            )
        );

    }

    /**
     * @param $plugin
     * @return Capture
     */
    protected function captureFactory($plugin)
    {
        $capture = new Capture(
            new ComparableFactory(),
            new DBSystem(
                new PostTypeCRUD()
            ),
            $plugin
        );
        return $capture;
    }


}