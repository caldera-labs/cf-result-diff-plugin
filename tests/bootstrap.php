<?php
if( ! defined( 'CF_RESULTS_DIFF_WP_VER' ) ){
    define( 'CF_RESULTS_DIFF_WP_VER', '0.0.1' );
}
spl_autoload_register(function($class) {

    $cfPath = dirname( __FILE__, 2  ) . '/vendor/Desertsnowman/caldera-forms/';
    $cfClasses = [
        'Caldera_Forms_Entry' => 'classes/entry.php'
    ];
    if( array_key_exists( $class, $cfClasses ) ){
        include_once $cfPath . $cfClasses[ $class ];
    }
});
include_once dirname( __FILE__, 2 ) . '/vendor/autoload.php';
include_once dirname( __FILE__, 2 ) . '/bootstrap.php';
