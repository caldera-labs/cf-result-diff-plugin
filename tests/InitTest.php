<?php


namespace calderawp\testing\resultDiffPlugin\Tests;


class InitTest extends TestCase
{

    /**
     * Test initialization
     *
     * @covers  \cfResultDiffInit()
     */
    public function testReturn()
    {
        $this->assertNotNull( \cfResultDiffInit() );
        $this->assertInstanceOf(
            \calderawp\testing\resultDiffPlugin\Plugin::class,
            \cfResultDiffInit()
        );
    }

    /**
     * Test initializing with a different app.
     *
     * @covers  \cfResultDiffInit()
     */
    public function testAppArg()
    {
        $app = $this->appFactory();
        $plugin = \cfResultDiffInit( $app );
        $this->assertSame(
            $app,
            $plugin->getApp()
        );
    }

    /**
     * Test events get registered when loading plugin via function
     *
     * @covers  \cfResultDiffInit()
     * @covers Plugin::pluginLoaded()
     * @covers Plugin::$capture
     * @covers Capture::addHooks()
     */
    public function testEventsRegistered()
    {
        $app = $this->appFactory();
        $this->assertFalse(
            $app->getEventsManager()->hasFilter(
                'caldera_forms_mailer'
            )
        );

        $this->assertFalse(
            $app->getEventsManager()->hasFilter(
                'init'
            )
        );

        $plugin = \cfResultDiffInit( $app );

        $this->assertTrue(
            $app->getEventsManager()->hasFilter(
                'caldera_forms_mailer'
            )
        );

        $this->assertTrue(
            $app->getEventsManager()->hasFilter(
                'init'
            )
        );
    }
}