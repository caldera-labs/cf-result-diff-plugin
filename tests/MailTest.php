<?php


namespace calderawp\testing\resultDiffPlugin\Tests;


use calderawp\testing\resultDiffPlugin\ComparableFactory;
use calderawp\testing\resultDiffPlugin\Plugin;
use calderawp\testing\resultDiffPlugin\TestMail;

class MailTest extends TestCase
{

    /**
     * Test the mock entity
     *
     * @covers MailTest::getMockDBEntity()
     */
    public function testEntityFactory()
    {
        $formId = uniqid( 'CF' );
        $entryId = 42;
        $this->assertEquals(
            TestMail::OFFSETDATA,
            $this->getMockDBEntity( TestMail::OFFSETDATA, $formId, $entryId )->getType()
        );

        $this->assertEquals(
            TestMail::OFFSETENTRY,
            $this->getMockDBEntity( TestMail::OFFSETENTRY, $formId, $entryId )->getType()
        );


    }

}


