<?php


namespace calderawp\testing\resultDiffPlugin;


use calderawp\testing\resultDiff\Comparisons\EmailArgs;
use calderawp\testing\resultDiff\Comparisons\EmailData;
use calderawp\testing\resultDiff\ArrayLike\Form;
use calderawp\testing\resultDiff\Comparisons\Entry;
use calderawp\interop\Entities\Entry as EntryEntity;

class ComparableFactory
{

    /**
     * Create Email Data Comparable
     *
     * @param array $data
     * @param Form $form
     *
     * @return EmailData
     */
    public function emailData( array $data, Form $form )
    {
        return new EmailData(
            $data,
            $form
        );

    }

    /**
     * Create Entry comparison
     *
     * @param EntryEntity $entryEntity
     * @param Form $form
     * @return Entry
     */
    public function entry( EntryEntity $entryEntity, Form $form )
    {
        return new Entry(
            $entryEntity,
            $form
        );
    }

    /**
     * Create EmailArgs comparison
     *
     * @param array $mailer
     * @param Form $form
     * @return EmailArgs
     */
    public function emailArgs( array  $mailer, Form $form )
    {
        return new EmailArgs(
            $mailer,
            $form
        );
    }

    /**
     * Get a form ArrayLike
     *
     * @param array $formConfig
     *
     * @return Form
     */
    public function form(array  $formConfig )
    {
        return new Form( $formConfig );
    }


}