<?php


namespace calderawp\testing\resultDiffPlugin;



use calderawp\testing\resultDiff\DB\Entity;
use calderawp\testing\resultDiff\EntityCompare;

class TestMail
{

    private $mail;
    private $data;
    private $form;
    private $entryId;
    private $plugin;

    /** @var string */
    const OFFSETDATA = 'emaildata';

    /** @var string */
    const OFFSETENTRY = 'entry';

    /** @var string */
    const OFFSETARGS = 'emailargs';

    /** @var  array */
    protected $results;

    /** @var DBSystem  */
    protected $DBSystem;

    /**
     * TestMail constructor.
     *
     * Designed to be used with "caldera_forms_mailer" filter
     *
     * @param array $mail
     * @param array $data
     * @param array $form
     * @param int|null $entryId
     * @param Plugin $plugin
     */
    public function __construct( $mail, $data, $form, $entryId, Plugin $plugin, DBSystem $DBSystem )
    {

        $this->mail = $mail;
        $this->data = $data;
        $this->form = $form;
        $this->entryId = $entryId;
        $this->plugin = $plugin;
        $this->DBSystem = $DBSystem;

    }

    /**
     * Get result by type
     *
     * @param string$type
     * @return bool|null
     */
    public function getResult( $type )
    {
        isset( $this->results[ $type ] )
            ? $result = $this->results[ $type ]
        : $result = null;
        return $result;
    }

    /**
     * Test entry
     *
     * @param Entity $last Last saved of type
     * @param ComparableFactory $comparableFactory Comparison factory to use
     *
     * @return \calderawp\interop\Entities\Entity
     */
    public function testEntry( Entity $last = null, ComparableFactory $comparableFactory )
    {
        $entry = new \Caldera_Forms_Entry( $this->form, $this->entryId );
        $entity = $this
            ->plugin
            ->getApp()
            ->createEntity(
                \calderawp\interopWP\Entities\Entry::class,
                [
                    $entry
                ]
            );



        $entryCompare = $comparableFactory->entry(
            $entity,
            $comparableFactory->form(
                $this->form
            )
        );

        $result = null;
        $entryCompareEntity = $entryCompare->toEntity();
        if( $last ){
            $compare = new EntityCompare(
                $entryCompareEntity,
                $last
            );

            $result = $compare->validate();
        }

        $this->DBSystem->getPostTypeCrud()->create( $entryCompareEntity );


        $this->results[ self::OFFSETENTRY ]  = $result;
        return $entryCompareEntity;
    }

    /**
     * Test email data -- the $data arg of caldera_forms_mailer filter
     *
     * @param Entity $last Last saved of type
     * @param ComparableFactory $comparableFactory Comparison factory to use
     * @return Entity
     */
    public function testEmailData( Entity $last = null , ComparableFactory $comparableFactory )
    {
        $emailData = $comparableFactory->emailData(
            $this->data,
            $comparableFactory->form( $this->form )
        );

        $emailDataEntity = $emailData->toEntity();
        $result = null;

        if( $last ){
            $compare = new EntityCompare(
                $emailDataEntity,
                $last
            );

            $result = $compare->validate();
        }

        $this->results[ self::OFFSETDATA ]  = $result;

        $this->DBSystem->getPostTypeCrud()->create( $emailDataEntity );

        return $emailDataEntity;

    }

    /**
     * Test that $mailer arg on caldera_forms_mailer is consistent.
     *
     * @param Entity|null $last
     * @param array $mailer
     * @param ComparableFactory $comparableFactory
     * @return \calderawp\testing\resultDiff\Comparisons\EmailArgs
     */
    public function testMailerArgs( Entity $last = null, array $mailer, ComparableFactory $comparableFactory )
    {
        $form = $comparableFactory->form( $this->form );
        $mailArgsCompare = $comparableFactory->emailArgs(
            $mailer,
            $form
        );

        $mailArgsCompareEntity = $mailArgsCompare->toEntity();
        $result = null;

        if( $last ){
            $compare = new EntityCompare(
                $mailArgsCompareEntity,
                $last
            );

            $result = $compare->validate();
        }

        $this->results[ self::OFFSETARGS ]  = $result;

        $this->DBSystem->getPostTypeCrud()->create( $mailArgsCompareEntity );

        return $mailArgsCompare;


    }
}