<?php


namespace calderawp\testing\resultDiffPlugin;


use calderawp\interop\Entities\Entry;

use calderawp\interop\Events\Event;
use calderawp\interop\Events\Events;
use calderawp\testing\resultDiff\Comparisons\Comparison;
use calderawp\testing\resultDiff\Comparisons\EmailData;
use calderawp\testing\resultDiffPlugin\Storage\PostType;

class Capture
{
    /** @var ComparableFactory  */
    protected $comparableFactory;

    /** @var DBSystem */
    protected $DBSystem;

    /** @var  Events */
    protected $events;

    /** @var  Plugin */
    protected $plugin;

    /** @var  TestMail */
    protected $testMail;


    public function __construct(ComparableFactory $comparableFactory, DBSystem $DBSystem, Plugin $plugin)
    {
        $this->comparableFactory = $comparableFactory;
        $this->DBSystem = $DBSystem;
        $this->plugin = $plugin;
    }

    /**
     * Register events with WordPress
     */
    public function addHooks()
    {

        $this->registerPostType();

        $events = $this->plugin->get( Plugin::OFFSETEVENTS );
        $events->addFilter(
            $this->mailerFilter()
        );

        $events->addAction(
            $this->createInitAction()
        );


    }

    /**
     * Get email test results
     *
     * @param string $type Type of result to get
     * @return bool|null
     */
    public function emailResults( $type )
    {
        if(  is_object( $this->testMail ) ){

            return $this->testMail->getResult( $type );
        }

    }

    /**
     * Check mailer against last
     *
     * @uses "caldera_forms_mailer" filter
     *
     * @param array $mailer
     * @param array $data
     * @param array $form
     * @param int|null $entryId
     * @return array
     */
    public function mailer($mailer, $data, $form, $entryId )
    {
        $this->testMail = new TestMail(
            $mailer,
            $data,
            $form,
            $entryId,
            $this->plugin,
            $this->DBSystem
        );

        $this->testEmailData( $form );

        $this->testMailArg( $form, $mailer );
        if( is_numeric( $entryId ) ){
            $this->testEntry( $form );
        }



        return $mailer;
    }

    protected function testMailArg( $form, $mailer )
    {
        $last = $this->lastOfType( TestMail::OFFSETARGS, $form[ 'ID' ] );

        $this->testMail->testMailerArgs(
            $last,
            $mailer,
            $this->comparableFactory
        );

        return $this->emailResults( TestMail::OFFSETARGS );
    }

    /**
     * Test entry is the same
     *
     * @param array $form
     * @return bool|null
     */
    protected function testEntry( $form )
    {

        $last = $this->lastOfType( TestMail::OFFSETENTRY, $form[ 'ID' ] );
        $this->testMail->testEntry(
            $last,
            $this->comparableFactory
        );

        return $this->emailResults( TestMail::OFFSETENTRY );

    }

    /**

     * @return bool|null True or false if validated, or null if no previous result was found
     */
    protected function testEmailData( $form )
    {
        $last = $this->lastOfType(TestMail::OFFSETDATA, $form[ 'ID'] );
        $this->testMail->testEmailData(
            $last,
            $this->comparableFactory
        );

        return $this->emailResults( TestMail::OFFSETDATA );

    }

    /**
     * Register our post type
     *
     * @uses "init" action
     */
    public function registerPostType()
    {
        if ( function_exists( 'register_post_type' ) ) {
            register_post_type(PostType::NAME, PostType::args());
            register_post_status( TestMail::OFFSETENTRY );
            register_post_status( TestMail::OFFSETDATA );
            register_post_status( TestMail::OFFSETARGS );
        }
    }

    /**
     * Create mailer filter event
     *
     * @return Event
     */
    protected function mailerFilter()
    {
        return Event::fromArray(
            [
                'name' => 'caldera_forms_mailer',
                'args' => 4,
                'priority' => 299,
                'callback' => [$this, 'mailer']
            ]
        );
    }

    /**
     * Create init action for registering post type.
     *
     * @return Event
     */
    protected function createInitAction()
    {
        return Event::fromArray(
            [
                'name' => 'init',
                'args' => 1,
                'priority' => 21,
                'callback' => [$this, 'registerPostType']
            ]
        );
    }

    /**
     * Get the last comparison of a type.
     *
     * Returns null if none found.
     *
     * @param string $type Type of compare to get.
     *
     * @return Comparison|null
     */
    protected function lastOfType( $type, $formId )
    {
        try {
            /** @var EmailData $last */
            return $this->DBSystem->getPostTypeCrud()->lastOfType( $type, $formId );
        } catch (\Exception $e) {
            return null;
        }

    }
}