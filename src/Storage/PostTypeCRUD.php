<?php


namespace calderawp\testing\resultDiffPlugin\Storage;


use calderawp\testing\resultDiff\DB\Entity;
use calderawp\testing\resultDiff\Interfaces\CRUD;

class PostTypeCRUD implements TypeCRUD
{

    /** @var string  */
    protected $formIdMetaKey = '_cf_result_diff_form_id';
    /**
     * @inheritdoc
     */
    public function lastOfType( $type, $formId )
    {
        global $wp_post_statuses;
        $query = new \WP_Query([
            'post_type' => PostType::NAME,
            'post_status' =>  $type,
            'orderby' => 'date',
            'posts_per_page' => 1,
            'meta_key' => $this->formIdMetaKey,
            'meta_value' => $formId
        ]);

        if( ! empty( $query->posts[0] ) ){
            return $this->fromPost(
                $query->posts[0]
            );
        }

        throw new \Exception(
            __( 'Not Found' )
        );

    }
    /** @inheritdoc */
    public function create( Entity $entity)
    {
        $array = $this->toPostArray( $entity );
        unset( $array[ 'ID' ] );

        $id = wp_insert_post( $array );

        if( ! is_wp_error( $id ) ) {
            update_post_meta( $id, $this->formIdMetaKey, $entity->getFormId() );
            $entity->setID( $id );
            return $entity;
        }

        throw new \Exception(
            $id->get_error_message( $id->get_error_code() )
        );

    }

    /** @inheritdoc */
    public function read( $id )
    {
        $found = get_post( $id );
        if( is_object( $found ) && $found->ID === $id ){
            return $this->fromPost( $found );
        }

        throw new \Exception(
            __( 'Entity not found or invalid', 'cf-result-diff-plugin' )
        );
    }

    /** @inheritdoc */
    public function update( Entity $entity)
    {

        $saved = wp_update_post( $this->toPostArray( $entity ) );

        if( ! is_wp_error( $saved ) ) {
            update_post_meta( $saved, $this->formIdMetaKey, $entity->getFormId() );

            return $entity;
        }

        throw new \Exception(
            $saved->get_error_message( $saved->get_error_code() )
        );
    }

    /** @inheritdoc */
    public function delete(Entity $entity)
    {
        return wp_delete_post( $entity->ID );
    }

    /**
     * Create entity from stored post
     *
     * @param \WP_Post $post Stored post
     * @return Entity
     */
    protected function fromPost( \WP_Post $post )
    {

        return new Entity(
            [
                'ID'            => $post->ID,
                'type'          => $post->post_status,
                'hash'          => $post->post_title,
                'formHash'      => $post->post_content,
                'formId'        => $post->post_excerpt,
            ]
        );

    }

    /**
     * Convert Entity to array we can pass to wp_insert/update_post()
     *
     * @param Entity $entity
     *
     * @return array
     */
    public function toPostArray(Entity $entity)
    {
        return [
            'post_title'    => $entity->getHash(),
            'post_content'  => $entity->getFormHash(),
            'post_excerpt'  => $entity->formId,
            'post_status'   => $entity->type,
            'post_type'     => PostType::NAME
        ];
    }

}