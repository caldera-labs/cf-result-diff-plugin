<?php


namespace calderawp\testing\resultDiffPlugin\Storage;


class PostType
{

    /**
     * Name of post type
     */
    const NAME = '_cf_result_diff';

    /**
     * Post type args
     *
     * @return array
     */
    public static function args()
    {
        return [
            'labels'             => [],
            'description'        => __( 'Description.', 'cf-result-difff' ),
            'public'             => true,
            'publicly_queryable' => false,
            'show_ui'            => true,
            'show_in_menu'       => true,
            'query_var'          => false,
            'capability_type'    => 'post',
            'has_archive'        => false,
            'hierarchical'       => false,
            'menu_position'      => null,
            'supports'           => [ 'title', 'editor' ]
        ];
    }

}