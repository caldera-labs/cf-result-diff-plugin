<?php


namespace calderawp\testing\resultDiffPlugin\Storage;


use calderawp\testing\resultDiff\DB\Entity;
use calderawp\testing\resultDiff\Interfaces\CRUD;

interface TypeCRUD extends CRUD
{
    /**
     * Get last test, by form ID and type
     *
     * @param string $type Type of test
     * @param string $formId ID of form
     * @return Entity
     * @throws \Exception
     */
    public function lastOfType( $type, $formId );

}