<?php


namespace calderawp\testing\resultDiffPlugin;


use calderawp\interop\App;
use calderawp\interop\Events\Events;
use calderawp\interop\Exceptions\ContainerException;
use calderawp\testing\resultDiffPlugin\Exceptions\Exception;
use calderawp\testing\resultDiffPlugin\Storage\PostTypeCRUD;
use Pimple\Container;

class Plugin implements \calderawp\interop\Interfaces\Plugin
{
    /** @var  App */
    protected $app;

    /** @var  string */
    protected $version;

    /** @var  string */
    protected $basePath;

    /** @var  array */
    protected $offsets;

    /** @var  array */
    protected $items;

    /** @var  Capture */
    protected $capture;

    /** @var string */
    const OFFSETPOSTYPECRUD = 'postTypeCrud';

    /** @var string */
    const OFFSETDB = 'db';

    /** @var string */
    const OFFSETEVENTS = 'events';

    /** @var string */
    const OFFSETCOMPARABLEFACTORY = 'comparableFactory';

    /** @var Container */
    protected $pimple;

    /**
     * Plugin constructor.
     * @param string $version
     * @param string $basePath
     */
    public function __construct( $version, $basePath )
    {
        $this->version = $version;
        $this->basePath = $basePath;
        $this->setupContainer();
    }

    /** @inheritdoc */
    public function pluginLoaded( Events $events )
    {
        if( ! is_object( $this->app ) ){
            throw new Exception( 'Result diff plugin loaded before app added to plugin' );
        }

        $this->capture = new Capture(
            $this->get( self::OFFSETCOMPARABLEFACTORY ),
            $this->get( self::OFFSETDB ),
            $this
        );

        $this->capture->addHooks();

    }

    /**
     * Reset DBSystem
     *
     * @param DBSystem $DBSystem
     */
    public function resetDBSystem( DBSystem $DBSystem )
    {
        $this->pimple[ self::OFFSETDB ] = $DBSystem;

    }

    /** @inheritdoc */
    public function get( $id )
    {
        if( $this->has( $id ) ){
            return $this->pimple[ $id ];
        }

        throw new ContainerException( $id );
    }

    /** @inheritdoc */
    public function has( $id ){
        return array_key_exists( $id, $this->allowedOffsets() );
    }

    /**
     * Get (lazy-creating) offsets/factories for underlying container
     *
     * @return array
     */
    protected function allowedOffsets()
    {
        if( empty( $this->offsets ) ){
            $this->offsets = [
                self::OFFSETDB => function( ){
                    return new DBSystem( $this->get( self::OFFSETPOSTYPECRUD ) );
                },
                self::OFFSETPOSTYPECRUD => function(){
                    return new PostTypeCRUD();
                },
                self::OFFSETEVENTS => function(){
                    return $this->app->getEventsManager();
                },
                self::OFFSETCOMPARABLEFACTORY => function(){
                    return new ComparableFactory();
                }
            ];
        }
        return $this->offsets;

    }

    /** @inheritdoc */
    public function version()
    {
        return $this->version;
    }

    /** @inheritdoc */
    public function getOverrideMap()
    {
        return [];
    }

    /** @inheritdoc */
    public function getNamespace()
    {
        return "calderawp\\testing\\resultDiffPlugin\\";
    }

    /** @inheritdoc */
    public function basePath()
    {
        return $this->basePath;
    }

    /** @inheritdoc */
    public function setApp(App $app)
    {
        $this->app = $app;
    }

    /**
     * @return App
     */
    public function getApp()
    {
        return $this->app;
    }
    /**
     * Add services to container
     */
    private function setupContainer()
    {
        $this->pimple = new Container();

        foreach ( $this->allowedOffsets() as $offset => $callback ){
            $this->pimple[ $offset ] =  $callback;

        }

    }

}