<?php


namespace calderawp\testing\resultDiffPlugin;


use calderawp\testing\resultDiffPlugin\Storage\PostTypeCRUD;
use calderawp\testing\resultDiffPlugin\Storage\TypeCRUD;

class DBSystem
{

    /**
     * @var PostTypeCRUD
     */
    private  $postTypeCrud;

    public function __construct( TypeCRUD $postTypeCrud )
    {
        $this->postTypeCrud = $postTypeCrud;
    }

    /**
     * @return PostTypeCRUD
     */
    public function getPostTypeCrud()
    {
        return $this->postTypeCrud;
    }
}