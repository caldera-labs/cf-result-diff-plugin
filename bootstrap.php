<?php
/**
 * If WordPress plugins API exists, load.
 */
if ( function_exists( 'add_action' ) ) {
    add_action('init',
        function () {
            include_once dirname(__FILE__) . '/vendor/autoload.php';
            cfResultDiffInit();

        },
        2
    );
}

/**
 * @param \calderawp\interop\InteropApp|null $app
 * @return \calderawp\testing\resultDiffPlugin\Plugin
 */
function cfResultDiffInit( \calderawp\interop\InteropApp $app = null )
{
    if ( ! $app ) {
        $app = \calderawp\interop\Interop();
    }

    $diffPlugin = new \calderawp\testing\resultDiffPlugin\Plugin(
        CF_RESULTS_DIFF_WP_VER,
        dirname(__FILE__)
    );

    $diffPlugin->setApp( $app );

    if (!$app->hasPluginByNamespace("calderawp\\interopWP\\")) {
        $interopWpPlugin = new \calderawp\interopWP\Plugin();
        $app->addPlugin($interopWpPlugin);
    }

    $app->addPlugin($diffPlugin);

    return $diffPlugin;
}
